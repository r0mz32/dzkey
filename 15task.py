def sum_range(a, z):
    if a > z:
        a, z = z, a
    return sum(range(a, z + 1))

def main():
    a=int(input('start '))
    b=int(input('end '))
    print(sum_range(a,b))
if __name__ == '__15task__':
    main()
