def palyndrome(input_str):
    without_gap=input_str.replace(' ', '')
    lower_case_without_gap=without_gap.lower()
    reversed_string = lower_case_without_gap[::-1]
    return lower_case_without_gap==reversed_string

def main():
    str_for_check=input('enter the string: ')
    if palyndrome(str_for_check):
        print("it's a palindrome")
    else:
        print("it's not a palindrome")

if __name__ == '__3task__':
    main()
    
