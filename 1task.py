def counter(func):
    def wrapper(*args, **kwargs):
        wrapper.count += 1
        return func(*args, **kwargs)
    wrapper.count = 0
    return wrapper

@counter
def print_hi(name):
    print(f'Hi, {name}')

if __name__ == '__1task__':
    print_hi('Boka')
    print_hi('Joka')
    print(print_hi.count)

