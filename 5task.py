import heapq

def main():
    my_dict = {1:21, 2:8848, 3:158, 4:44, 5:158, 6:666}
    print(heapq.nlargest(3, my_dict, key=my_dict.__getitem__))

if __name__ == '__5task__':
    main()
