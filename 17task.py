import math
class Point:
    def __init__(self, x = 0, y = 0):
        self.x = x
        self.y = y

    def _Info(self):
        print('I am a point, an abstract object in space, having neither volume, nor area, nor length, nor any other measurable characteristics. I am a zero-dimensional object.'
            )

    def get_x(self):
        return self.x

    def get_y(self):
        return self.y

    def set_x(self, x):
        self.x = x

    def set_y(self, y):
        self.y = y

    def essence(self):
        print('I am a I am an important part of mathematics')

class Line(Point):
    def __init__(self, x = 0, y = 0, x1=1,y1=1):
        Point.__init__(self,x,y)
        self.x1 = x1
        self.y1 = y1

    def _Info(self):
        print('I am a line, I only have a length, I am a one-dimensional object')

    def get_x1(self):
        return self.x1

    def get_y1(self):
        return self.y1

    def set_x1(self, x1):
        self.x1 = x1

    def set_y1(self, y1):
        self.y1 = y1

    def essence(self):
        print(super().essence())

    def Length(self):
        return math.hypot(self.x1 - self.x, self.y1 - self.y)