def date(day, month, year):
    if year % 4 == 0 and year % 100 != 0 or year % 400 == 0:
        month_day = {1:31, 2:29, 3:31, 4:30, 5:31,
                     6:30, 7:31, 8:31, 9:30, 10:31,
                     11:30, 12:31}
    else:
        month_day = {1: 31, 2: 28, 3: 31, 4: 30, 5: 31,
                     6: 30, 7: 31, 8: 31, 9: 30, 10: 31,
                     11: 30, 12: 31}
    if year > 0 and (month >= 1 and month <= 12):
        if day in range(1, month_day[month] + 1):
            print('Верная дата')
        else:
            print('неверная  дата')
    else:
        print('неверная  дата')

def main():
    user_day=int(input('Enter day: '))
    user_month=int(input('Enter month: '))
    user_year=int(input('Enter year: '))
    date(user_day, user_month, user_year)

if __name__ == '__11task__':
    main()
